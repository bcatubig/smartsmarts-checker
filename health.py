#!/usr/bin/env python
# Python SmartStats reader

import os, re, csv, subprocess

class colors:
    FAIL = '\033[0;31m' # RED
    SUCCESS = '\033[0;32m' # GREEN
    ENDC = '\033[0m' # Default

def runCommand(cmd):
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = p.communicate()
    except:
        return None
    if p.returncode == 0:
        return output[0]
    else:
        return None
        
def openFile(input):
    try:
        file = open(input, 'r')
        result = file.read()
        file.close()
        return result
    except:
        print "[-] Could not open the file: %s" % input

def checkRaid():
    # Check if debian or ubuntu
    if os.path.isfile('/etc/debian_version'):
        lspci = '/usr/bin/lspci'
    else:
        lspci = '/sbin/lspci'

    output = runCommand([lspci, '-mm'])
    print output

def main():
    print "Checking drive health stats..."
    
    # Check to see if server is using a Raid
    checkRaid()

    # Get output of smartstats
    smart_stats = runCommand(['/usr/sbin/smartctl', '-a', '/dev/sda'])
    
    # Parse SmartStats

    # Output Information to user about drive(s)

if __name__ == '__main__':
    main()
